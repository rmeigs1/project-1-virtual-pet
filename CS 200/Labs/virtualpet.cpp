#include <iostream>		// imports cin / cout
#include <string>		// imports string types
using namespace std;	// standard library

int main() {
	// Declaration and initiation
	int hunger = 0;
	int health = 100;
	int happiness = 100;
	string petName = "";
	bool isDone = false;
	int menuChoice;
	int foodMenuChoice;
	int gameMenuChoice;
	int quitMenuChoice;

	cout << "Please enter your pet's name: ";
	cin >> petName;
	cout << endl;

	while (!isDone) {		// While the game isn't done yet...
		cout << "----------------------------------------------------------------------------------" << endl;
		cout << petName << endl;
		cout << "\tHunger: " << hunger << "%" << "\tHealth: " << health << "%" << "\tHappiness: " << happiness << "%" << endl;
		cout << "----------------------------------------------------------------------------------" << endl;
	
		cout << "OPTIONS:\t1. Feed \t2. Play \t 3. Vet \t 4. Quit" << endl;
		cout << "> ";
		cin >> menuChoice;
		cout << endl;

		// Main menu
		switch (menuChoice) {
			case 1:
				cout << "FOODS:\t1. Pizza \t2. Broccoli \t 3. Tuna" << endl;
				cout << "> ";
				cin >> foodMenuChoice;
				cout << endl;
				if (foodMenuChoice == 1) {
					health -= 1;
					hunger -= 15;
					cout << "You feed pizza to " << petName << endl;
					cout << endl;
				}
				else if (foodMenuChoice == 2) {
					health += 1;
					hunger -= 10;
					cout << "You feed broccoli to " << petName << endl;
					cout << endl;
				}
				else if (foodMenuChoice == 3) {
					health += 2;
					hunger -= 12;
					cout << "You feed tuna to " << petName << endl;
					cout << endl;
				}
				break;
			case 2:
				cout << "GAMES:\t1. Fetch \t2. Tug-of-war \t 3. Videogame" << endl;
				cout << "> ";
				cin >> gameMenuChoice;
				cout << endl;
				if (gameMenuChoice == 1) {
					happiness += 8;
					health += 2;
					cout << "You play fetch with " << petName << endl;
					cout << endl;
				}
				else if (gameMenuChoice == 2) {
					happiness += 9;
					cout << "You play tug-of-war with " << petName << endl;
					cout << endl;
				}
				else if (gameMenuChoice == 3) {
					happiness += 10;
					health -= 1;
					cout << "You play videogames with " << petName << endl;
					cout << endl;
				}
				break;
			case 3:
				cout << "The vet says:" << endl;
				if (happiness < 50) {
					cout << "- Make sure to play with " << petName << " more." << endl;
				}
				if (hunger > 50) {
					cout << "- Make sure to feed " << petName << "." << endl;
				}
				if (health < 50) {
					cout << "- " << petName << " isn't looking healthy.  Take better car of " << petName << "!" << endl;
				}
				if ((happiness >= 50) && (hunger <= 50) && (health >= 50)) {
					cout << "- " << petName << " is looking OK!" << endl;
				}
				cout << endl;
				break;
			case 4:
				cout << "Are you sure want to quit?" << endl;
				cout << "1. QUIT \t2. Don't quit" << endl;
				cout << "> ";
				cin >> quitMenuChoice;
				if (quitMenuChoice == 1) {
					isDone = true;
				}
				break;
			default:
				cout << "Invalid command" << endl;
		}

		// Normal happiness and health decrementing
		hunger += 5;
		if (hunger > 50) {
			happiness -= 10;
			health -= 10;
		}
		else {
			happiness -= 5;
		}

		// Number bounding and game over state
		if (happiness < 0) {
			happiness = 0;
		}
		else if (happiness > 100) {
			happiness = 100;
		}
		
		if (hunger < 0) {
			hunger = 0;
		}
		else if (hunger > 100) {
			hunger = 100;
		}

		if (health > 100) {
			health = 100;
		}
		else if (health < 0) {
			health = 0;
			cout << "You haven't taken care of " << petName << "!" << endl << petName << " has been removed from your care." << endl;
			isDone = true;
		}
	}

	return 0;
}